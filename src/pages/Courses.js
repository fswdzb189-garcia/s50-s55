import {Fragment} from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';


export default function Courses() {

	// console.log(coursesData)
	// console.log(coursesData[0])

// loop here using map, foreach may not work
	const courses = coursesData.map(course => {
		return(

			<CourseCard key={course.name} courseProp={course}/>
			)

	})


	return(

		
		<Fragment>
			{/* <CourseCard courseProp={coursesData[0]}/> */}
			{courses}
		</Fragment>


		)
}